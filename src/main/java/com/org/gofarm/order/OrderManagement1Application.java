package com.org.gofarm.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class OrderManagement1Application {

	public static void main(String[] args) {
		SpringApplication.run(OrderManagement1Application.class, args);
	}

}
